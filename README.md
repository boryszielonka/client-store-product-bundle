Bundle is compatible with Symfony 3.3

## 1
composer.json
```json
    "repositories" : [
        {
        "type" : "vcs",
        "url" : "https://gitlab.com/boryszielonka/client-store-product-bundle.git"
        }
    ],
```

```sh
composer require guzzlehttp/guzzle &&
composer require boryszielonka/client-store-product-bundle dev-master
```

## 2
app/AppKernel.php
```php
    new BorysZielonka\ClientStoreProductBundle\BorysZielonkaClientStoreProductBundle()
```

## 3
app/config/config.yml
```yml
borys_zielonka_client_store_product:
    api_store_uri: '{API_STORE_URI}' // np. 'http://localhost:8000/api/product/'
```

## 4
app/config/routing.yml
```yml
    borys_zielonka_client_store_product:
    resource: "@BorysZielonkaApiStoreProductBundle/Controller/"
    type:     annotation
    prefix:   /
